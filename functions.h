#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

const char * func1();
const char * func2();

#ifdef __cplusplus
}
#endif
#endif