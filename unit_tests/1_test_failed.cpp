#include <string.h>
#include ".admin_files/test_utils.hpp"
#include "functions.h"

int
main(int argc, const char *argv[])
{
    return test_wrapper(argc, argv, []()
    {
        return strcmp("world", func2()) == 0;
    }); 
}
