#include <string.h>
#include ".admin_files/test_utils.hpp"
#include "functions.h"

int test()
{
    return strcmp("Hello", func1()) == 0;
}

int main(int argc, const char *argv[])
{
    return test_wrapper(argc, argv, (TestHandler) test);
}
