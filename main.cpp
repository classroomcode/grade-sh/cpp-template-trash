#include <stdio.h>
#include <errno.h>

static int
readline(char buf[], int size)
{   
    char c;
    int i = 0;
    do {
        if (i < size && ! feof(stdin)) {
            c = buf[i++] = fgetc(stdin); 
        }
        else {
            errno = ENOBUFS;
            return -1;
        }
    } while (c != '\n');
    buf[i-1] = 0;
    return i;
}

int 
main()
{
    while (! feof(stdin)) {
        char buffer[1024];
        if (readline(buffer, sizeof(buffer)) < 0) {
            perror("readline");
            return 1;
        }
        else {
            puts(buffer);
        }

    }
}